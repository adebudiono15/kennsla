import { Col, Container, Row } from "react-bootstrap"
import Layouts from "../Layouts"
import styles from './getStyles.module.css'

const NavbarComponent2 = () => {
  return (
    <Layouts title="Próf">
        <Container>
        <Row className={styles.container}>
            <Col lg={1} md={1} sm={1} xs={1}>
                <div className={styles.chevron}>
                    <i className='bx bx-sm bx-chevron-left'></i>
                </div>
            </Col>
            <Col lg={1} md={1} sm={1} xs={1} className={styles.breadcrumb}>
               <p className={styles.textBreadcrumb}>Til baka</p>
            </Col>
            <Col lg={8} md={8} sm={8} xs={8}>
                <p className={styles.textTitle}>Stafræn kennsla</p>
            </Col>
            <Col lg={2} md={2} sm={2} xs={2} style={{textAlign:'right'}}>
            <i className='bx bx-sm bx-plus' style={{rotate:'45deg'}}></i>
            </Col>
        </Row>
        </Container>
        <Container fluid className={styles.containerLine}/>
    </Layouts>
  )
}

export default NavbarComponent2