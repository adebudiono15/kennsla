import Image from 'next/image';
import { Container,Row,Col, Button } from 'react-bootstrap';
import styles from './getStyles.module.css';
import {ImgLogo} from '../../assets/images'

const FooterComponent = () => {
  return (
   <Container fluid className={styles.container}>
    <Container className={styles.wrapperContent}>
        <Row style={{justifyContent:'space-between'}}>
            <Col lg={6} md={6}>
              <p className={styles.textTitle}>Ertu komin/nn/ð á leiðarenda?</p>
              <p className={styles.textSubTitle}>Skoðaðu niðurstöðurnar þínar hér</p>
            </Col>
            <Col lg={6} md={6} className={styles.wrapperButton}>
            <Button className={styles.button}>Skoða niðurstöður</Button>
            </Col>
        </Row>
    </Container>

   </Container>
  )
}

export default FooterComponent