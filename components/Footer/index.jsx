import Image from 'next/image';
import { Container,Row,Col } from 'react-bootstrap';
import styles from './getStyles.module.css';
import {ImgLogo} from '../../assets/images'

const FooterComponent = () => {
  return (
   <Container fluid className={styles.container}>
    <Container className={styles.wrapperContent}>
        <Row>
            <Col lg={3} md={3}>
            <Image src={ImgLogo}   
            alt="Logo"
            className={styles.imgLogo}/>
            </Col>
            <Col lg={3} md={3}>
            <p className={styles.textTitle}>SETBERG{` - `}HÚS KENNSLUNNAR</p>
            <p  className={styles.textSubTitle}>Suðurgata 43</p>
            <p className={styles.textSubTitle}>inngangur við Sæmundargötu</p>
            <p className={styles.textSubTitle}>102 Reykjavík</p>
            </Col>
            <Col lg={3} md={3} sm={6} xs={6}>
            <div className={styles.gapText}/>
            <p  className={styles.textSubTitle}>525 4002</p>
            <p className={styles.textSubTitle}>525 5833</p>
            <p className={styles.textSubTitle}>kennslusvid@hi.is</p>
            </Col>
            <Col lg={3} md={3} sm={6} xs={6}>
            <div className={styles.gapText}/>
            <p  className={styles.textSubTitle}>Opið</p>
            <p className={styles.textSubTitle}>mán{`-`}fös </p>
            <p className={styles.textSubTitle}>9{`-`}15</p>
            </Col>
        </Row>
    </Container>

   </Container>
  )
}

export default FooterComponent