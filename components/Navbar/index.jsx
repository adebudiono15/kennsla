import {Container,Nav,Navbar,NavDropdown,Row,Col} from 'react-bootstrap';
import Image from 'next/image'
import {ImgLogo} from '../../assets/images'
import styles from './getStyles.module.css';
import Link from 'next/link';
import {Icon1,Icon2,Icon3,Icon4} from '../../assets/icons'

const NavbarComponent = () => {
  return (
      <Navbar bg="white" className={styles.navbar}>
      <Container>
        <Navbar.Brand href="#home" className={styles.navbarBrand}>
        <Image
            src={ImgLogo}
            alt="Logo"
            className={styles.imgLogo}
            />
            <div className={styles.line}/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Link href="#" className={styles.link}>
            <Nav.Link className={styles.textLink}>FORSÍÐA</Nav.Link>
            </Link>
            <NavDropdown title="VERKFÆRI" id="basic-nav-dropdown" className={styles.navDropDown}>
              <Container>
              <Row>
                <Col lg={6} md={6} sm={6} xs={6}>
                  <Row>
                    <Col lg={1} md={1} sm={1}>
                    <Image
                    alt="icon"
                    src={Icon1}
                    className={styles.icon}
                    />
                    </Col>
                    <Col lg={10} md={10} sm={10}>
                      <p className={styles.title}>Canvas<span><i class='bx bx-fw bx-chevron-right mt-0 mx-1' ></i></span></p>
                      <p className={styles.textDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                      </Col>
                  </Row>
                </Col>
                {/*  */}
                <Col lg={6} md={6} sm={6} xs={6}>
                  <Row>
                    <Col lg={1} md={1} sm={1} xs={6}>
                    <Image
                    alt="icon"
                    src={Icon1}
                    className={styles.icon}
                    />
                    </Col>
                    <Col lg={10} md={10} sm={10} xs={10}>
                      <p className={styles.title}>Canvas Studio<span><i class='bx bx-fw bx-chevron-right mt-0 mx-1' ></i></span></p>
                      <p className={styles.textDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                      </Col>
                  </Row>
                </Col>
              </Row>
              {/*  */}
              <Row className="mt-3">
                <Col lg={6} md={6} sm={6} xs={6}>
                  <Row>
                    <Col lg={1} md={1} sm={1}>
                    <Image
                    alt="icon"
                    src={Icon2}
                    className={styles.icon}
                    />
                    </Col>
                    <Col lg={10} md={10} sm={10}>
                      <p className={styles.title}>Feedback Fruits<span><i class='bx bx-fw bx-chevron-right mt-0 mx-1' ></i></span></p>
                      <p className={styles.textDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                      </Col>
                  </Row>
                </Col>
                {/*  */}
                <Col lg={6} md={6} sm={6} xs={6}>
                  <Row>
                    <Col lg={1} md={1} sm={1} xs={1}>
                    <Image
                    alt="icon"
                    src={Icon3}
                    className={styles.icon}
                    />
                    </Col>
                    <Col lg={10} md={10} sm={10} xs={10}>
                      <p className={styles.title}>Panopto<span><i class='bx bx-fw bx-chevron-right mt-0 mx-1' ></i></span></p>
                      <p className={styles.textDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                      </Col>
                  </Row>
                </Col>
              </Row>
              {/*  */}
              <Row className="mt-3">
                <Col lg={6} md={6} sm={6}>
                  <Row>
                    <Col lg={1} md={1} sm={1}>
                    <Image
                    alt="icon"
                    src={Icon4}
                    className={styles.icon}
                    />
                    </Col>
                    <Col lg={10} md={10} sm={10}>
                      <p className={styles.title}>Teams<span><i class='bx bx-fw bx-chevron-right mt-0 mx-1' ></i></span></p>
                      <p className={styles.textDesc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                      </Col>
                  </Row>
                </Col>
              </Row>
              </Container>
            </NavDropdown>
          </Nav>
          <Nav className="mx-auto">
            <Link href="#" className={styles.link}>
            <Nav.Link className={styles.textLink}>EN</Nav.Link>
            </Link>
            <Link href="#" className={styles.link}>
            <Nav.Link className={styles.textLink}>ÍS</Nav.Link>
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default NavbarComponent