import Layouts from '../../components/Layouts'
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'
import { Container,Row,Col, Button,Nav } from 'react-bootstrap'
import Image from 'next/image'
import styles from './getStyles.module.css'
import {Icon1,Icon2,Icon3,Icon4} from '../../assets/icons'

const Results = () => {
  return (
    <Layouts title="Results">
        <Navbar/>
          {/*  */}
        <Container fluid className={styles.containerContent}>
           <Container>
                <Row>
                        <Col lg={6} md={6} sm={6}>
                            <h1 className={styles.textTagline}>Vel gert!</h1>
                            <p className={styles.textTagDesc}>Hér fyrir neðan eru þínar niðurstöður. Til hliðar er kóði sem gott er að geyma, með honum er alltaf hægt að nálgast niðurstöðurnar. Ef þú vilt fá nánari aðstoð getur þú bókað viðtal hjá Kennslusviði.</p>
                        </Col>
                        <Col lg={6} md={6} sm={6}>
                         <div className={styles.cardBlue}>
                            <p className={styles.titleCardBlue}>Þinn kóði</p>
                            <p className={styles.titleCopyCardBlue}>QD789M67 <span><i class='bx bx-copy'></i></span></p>
                            <p className={styles.titleDescCopyCardBlue}>Hér er kóði sem gott er að geyma, með honum er alltaf hægt að nálgast niðurstöðurnar</p>
                         </div>
                         <Row className={styles.wrapperButton}>
                            <Col lg={6} md={6} sm={6} xs={6}>
                            <Button className={styles.button}><i class='bx bx-envelope'></i>NIÐURSTÖÐUR Í TÖLVUPÓSTI</Button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={6} className={styles.colButtonRight}>
                            <Button className={styles.button}><i class='bx bx-envelope'></i>NIÐURSTÖÐUR Í TÖLVUPÓSTI</Button>
                            </Col>
                         </Row>
                        </Col>
                    </Row>
           </Container>
        </Container>
        {/*  */}
        <Container className={styles.wrapperContainerContent}>
            <Row>
                <Col lg={3} md={3} sm={3} className={styles.colTabs}>
                <Nav defaultActiveKey="/Results" className="flex-column">
                    <Nav.Link className={styles.tabsActive} href="">MIÐLUN</Nav.Link>
                    <Nav.Link className={styles.tabsNonActive} href="">VERKEFNI OG PRÓF</Nav.Link>
                    <Nav.Link className={styles.tabsNonActive} href="">NÁMSSAMFÉLAGIÐ</Nav.Link>
                    </Nav>
                </Col>
                <Col lg={9} md={9} sm={9} className={styles.colTabsLeft}>
                    <Row>
                        <Col>
                        <p className={styles.titleCardRight}>MIÐLUN</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon1}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Stutt kennslumyndbönd</span>
                                <p className={styles.titleCardRightContent}>Canvas studio</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row className="mt-5">
                        <Row>
                            <Col>
                            <p className={styles.titleCardRight}>VERKEFNI OG PRÓF</p>
                            </Col>
                        </Row>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon1}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Umræður á neti</span>
                                <p className={styles.titleCardRightContent}>Canvas</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon4}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Hópavinna</span>
                                <p className={styles.titleCardRightContent}>Teams</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row className="mt-5">
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon1}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Umræður á neti</span>
                                <p className={styles.titleCardRightContent}>Canvas</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon4}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Hópavinna</span>
                                <p className={styles.titleCardRightContent}>Teams</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row className="mt-5">
                        <Row>
                            <Col>
                            <p className={styles.titleCardRight}>NÁMSSAMFÉLAGIÐ</p>
                            </Col>
                        </Row>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon1}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Umræður á neti</span>
                                <p className={styles.titleCardRightContent}>Canvas</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon4}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Hópavinna</span>
                                <p className={styles.titleCardRightContent}>Teams</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
        {/*  */}
        <Footer/>
    </Layouts>
  )
}

export default Results