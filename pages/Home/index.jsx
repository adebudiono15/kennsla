import { Button, Col, Container, Row,Form } from 'react-bootstrap'
import Layouts from '../../components/Layouts'
import NavbarComponent from '../../components/Navbar'
import FooterComponent from '../../components/Footer'
import styles from './getStyles.module.css'
import Link from 'next/link'
const Index = () => {
  return (
    <Layouts title="Home">
      <NavbarComponent/>
      <Container fluid className={styles.header}>
        {/*  */}
        <Container>
              <Row className='mt-5'>
                <Col lg={5}>
                <div className={styles.contentHeader}>
                  <div className={styles.wrapperTagline}>
                  <h1 className={styles.textTagline}>Stafræn kennsla</h1>
                  </div>
                  <p className={styles.textDescTagline}>Það getur verið flókið að búa til stafrænt námskeið, hvaða forrit á að velja, hvernig eiga verkefnaskil að fara fram og hvernig á að virkja nemendur? Á þessari síðu getur þú prófað þig áfram og fundið þá leið sem hentar þér og þínum nemendum best.</p>
                 <Link href="/Prof" className={styles.ctaHeader}>
                 <span>TAKTU PRÓFIÐ</span>
                 </Link>
                </div>
            </Col>
              </Row>
        </Container>
        {/*  */}
        <div className={styles.gap120}/>
        {/*  */}
      <Container className={styles.cardHeader}>
          <Row>
            <Col lg={8} md={8} sm={8}>
                <h3 className={styles.textTitleCard}>Hefurðu tekið prófið áður?</h3>
                <p className={styles.textSubTitleCard}>Stimplaðu inn þinn kóða til að sjá niðurstöðurnar. </p>
            </Col>
            <Col  lg={4} md={4} sm={12} style={{placeSelf:'center'}}> 
            <Row>
              <Col lg={10} md={10} sm={10} xs={10}>
              <Form>
                    <Form.Control className={styles.formSeacrh} type="text" placeholder="Eg. QDM67819E" />
                </Form>
              </Col>
              <Col lg={2} md={2} sm={2} xs={2}>
              <Button className={styles.buttonSeacrh}>{`>`}</Button>
              </Col>
            </Row>
            </Col>
          </Row>
        </Container>
      </Container>
      <FooterComponent/>
    </Layouts>
    )
}

export default Index