import Layouts from '../../components/Layouts'
import Navbar from '../../components/Navbar'
import Footer from '../../components/Footer'
import { Container,Row,Col, Button,Nav } from 'react-bootstrap'
import Image from 'next/image'
import styles from './getStyles.module.css'
import {Icon1,Icon2,Icon3,Icon4} from '../../assets/icons'

const Verkfaeri = () => {
  return (
       <Layouts title="Verkfaeri">
       <Navbar/>
          {/*  */}
        <Container fluid className={styles.containerContent}>
           <Container>
                <Row>
                        <Col lg={6} md={6} sm={6}>
                            <h1 className={styles.textTagline}>Feedback Fruits</h1>
                            <p className={styles.textTagDesc}><b>FeedbackFruits</b> er safn verkfæra sem hefur verið bætt inn í HÍ Canvas. Þau er hægt að nota í gagnvirka kennslu, við endurgjöf og jafningjamat og til að bæta samvinnumöguleikum við efni í Canvas.</p>
                        </Col>
                        <Col lg={6} md={6} sm={6} className='text-center'>
                                <Image 
                                alt="icon"
                                src={Icon2}
                                className={styles.imgHeader}
                                />
                        </Col>
                    </Row>
           </Container>
        </Container>
        {/*  */}
        <Container className={styles.wrapperContainerContent}>
            <Row>
                <Col lg={3} md={3} sm={3} className={styles.colTabs}>
                <Nav defaultActiveKey="/Verkfaeri" className="flex-column">
                    <Nav.Link className={styles.tabsActive} href="">MIÐLUN</Nav.Link>
                    <Nav.Link className={styles.tabsNonActive} href="">VERKEFNI OG PRÓF</Nav.Link>
                    <Nav.Link className={styles.tabsNonActive} href="">NÁMSSAMFÉLAGIÐ</Nav.Link>
                    </Nav>
                </Col>
                <Col lg={9} md={9} sm={9} className={styles.colTabsLeft}>
                    <Row>
                        <Col>
                        <p className={styles.titleCardRight}>MIÐLUN</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Gagnvirk kynning</p>
                                <p className={styles.titleBlueCardRightContent}>Interactive presentation</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Teymisnám</p>
                                <p className={styles.titleBlueCardRightContent}>Team based learning</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row className='mt-5'>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Gagnvirk kynning</p>
                                <p className={styles.titleBlueCardRightContent}>Interactive presentation</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Teymisnám</p>
                                <p className={styles.titleBlueCardRightContent}>Team based learning</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row className='mt-5'>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Gagnvirk kynning</p>
                                <p className={styles.titleBlueCardRightContent}>Interactive presentation</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Teymisnám</p>
                                <p className={styles.titleBlueCardRightContent}>Team based learning</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row  className='mt-5'>
                        <Col>
                        <p className={styles.titleCardRight}>VERKEFNI OG PRÓF</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Gagnvirk kynning</p>
                                <p className={styles.titleBlueCardRightContent}>Interactive presentation</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Teymisnám</p>
                                <p className={styles.titleBlueCardRightContent}>Team based learning</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                    {/*  */}
                    <Row className='mt-5'>
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Gagnvirk kynning</p>
                                <p className={styles.titleBlueCardRightContent}>Interactive presentation</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                        {/*  */}
                        <Col lg={6} md={6} sm={6}>
                            <div className={styles.cardRight}>
                                <Image
                                alt='logo'
                                src={Icon2}
                                className={styles.img}
                                />
                                <span className={styles.badge}>Gagnvirkar glærukynningar</span>
                                <p className={styles.titleCardRightContent}>Teymisnám</p>
                                <p className={styles.titleBlueCardRightContent}>Team based learning</p>
                                <p className={styles.descCardRightContent}>Með Canvas Studio er hægt að taka upp það sem gerist á tölvuskjánum, notandann sjálfan í mynd eða hvoru tveggja í senn. Studio býður upp á að klippa myndbönd, texta, lesa inn á, hækka/lækka hljóð og setja skýringar, örvar og fleira inn á myndbandið.</p>
                                <Button className={styles.buttonCardRight}>Skoða nánar</Button>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
        {/*  */}
        <Footer/>
       </Layouts>
  )
}

export default Verkfaeri