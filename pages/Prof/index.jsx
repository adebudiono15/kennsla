import Navbar2 from '../../components/Navbar2'
import Footer2 from '../../components/Footer2'
import { Container,Row,Col } from 'react-bootstrap'
import Image from 'next/image'
import styles from './getStyles.module.css'
import {Icon1,Icon2,Icon3,Icon4} from '../../assets/icons'

const Prof = () => {
  return (
    <div>
        <Navbar2/>
          {/*  */}
          <Container className={styles.containerContent}>
            <Row>
                <Col className={styles.wrapper} lg={6} md={6} sm={6}>
                <div className={styles.lineContent}/>
                    <Row>
                        <Col>
                        <div className={styles.elipse}>
                            <span className={styles.textNumber}>1</span>
                        </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h3 className={styles.title}>Hvernig viltu miðla námsefni?</h3>
                            <div className={styles.wrapperCardContent}>
                              <div>
                              <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                                <span className={styles.textContent}>Stutt kennslumyndbönd</span>
                              </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                            <div>
                                <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                <span className={styles.textContent}>Skjöl og skrár</span>
                            </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>  
                                     <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Kennsla í gegnum fjarfundi</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>
                                    <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Upptökur af fyrirlestri</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>
                                    <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Gagnvirkar glærukynningar</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col className={styles.wrapperCardRight} lg={6} md={6} sm={6}>
                <p className={styles.titleCardRight}>Tæki og tól sem nýtast kennslunni</p>
                <Row>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRight}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon1}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    {/*  */}
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRight}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon1}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas Studio</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/*  */}
                <Row className='mt-3'>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon2}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    {/*  */}
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon3}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas Studio</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/*  */}
                <Row className='mt-3'>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon4}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                </Col>
            </Row>
        </Container>
        {/*  */}
        <Container className={styles.containerContent}>
            <Row>
                <Col className={styles.wrapper} lg={6} md={6} sm={6}>
                <div className={styles.lineContent}/>
                    <Row>
                        <Col>
                        <div className={styles.elipse}>
                            <span className={styles.textNumber}>2</span>
                        </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h3 className={styles.title}>Hvað viltu leggja fyrir?</h3>
                            <div className={styles.wrapperCardContent}>
                              <div>
                              <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                                <span className={styles.textContent}>Stutt kennslumyndbönd</span>
                              </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                            <div>
                                <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                <span className={styles.textContent}>Skjöl og skrár</span>
                            </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>  
                                     <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Kennsla í gegnum fjarfundi</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>
                                    <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Upptökur af fyrirlestri</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>
                                    <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Gagnvirkar glærukynningar</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col className={styles.wrapperCardRight} lg={6} md={6} sm={6}>
                <p className={styles.titleCardRight}>Tæki og tól sem nýtast kennslunni</p>
                <Row>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRight}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon1}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    {/*  */}
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRight}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon1}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas Studio</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/*  */}
                <Row className='mt-3'>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon2}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    {/*  */}
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon3}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas Studio</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/*  */}
                <Row className='mt-3'>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon4}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                </Col>
            </Row>
        </Container>
        {/*  */}
        <Container className={styles.containerContent}>
            <Row>
                <Col className={styles.wrapper} lg={6} md={6} sm={6}>
                <div className={styles.lineContent}/>
                    <Row>
                        <Col>
                        <div className={styles.elipse}>
                            <span className={styles.textNumber}>3</span>
                        </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h3 className={styles.title}>Hvernig viltu virkja nemendur?</h3>
                            <div className={styles.wrapperCardContent}>
                              <div>
                              <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                                <span className={styles.textContent}>Stutt kennslumyndbönd</span>
                              </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                            <div>
                                <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                <span className={styles.textContent}>Skjöl og skrár</span>
                            </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>  
                                     <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Kennsla í gegnum fjarfundi</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>
                                    <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Upptökur af fyrirlestri</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                            {/*  */}
                            <div className={styles.wrapperCardContent}>
                                <div>
                                    <i class='bx bx-sm bx-fw bxs-circle' style={{color:'#F5F5F5'}}></i>
                                    <span className={styles.textContent}>Gagnvirkar glærukynningar</span>
                                </div>
                                <i class='bx bx-chevron-down bx-sm'></i>
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col className={styles.wrapperCardRight} lg={6} md={6} sm={6}>
                <p className={styles.titleCardRight}>Tæki og tól sem nýtast kennslunni</p>
                <Row>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRight}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon1}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    {/*  */}
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRight}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon1}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas Studio</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/*  */}
                <Row className='mt-3'>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon2}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    {/*  */}
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon3}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas Studio</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                {/*  */}
                <Row className='mt-3'>
                    <Col lg={6} md={6} sm={12}>
                        <div className={styles.cardRightNonActive}>
                            <Row>
                                <Col lg={12} md={2} sm={2}>
                                <Image
                                alt="canvas"
                                src={Icon4}
                                className={styles.iconRight}
                                />
                                </Col>
                                <Col>
                            <div style={{justifyContent:'space-between',display:'flex'}}>
                                <span className={styles.textTitleCardRight}>Canvas</span> 
                                    <i className='bx bx-sm bx-fw bxs-check-circle' style={{color:'#0F0A9B'}}></i>
                            </div>
                                <p className={styles.textSubTitleCardRight}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sapien nunc, commodo.</p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
                </Col>
            </Row>
        </Container>
        {/*  */}
        <Footer2/>
    </div>
  )
}

export default Prof