import React, {useState,useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/app.css'

function MyApp({ Component, pageProps }) {
  const [pageLoaded,setPageLoaded] = useState(false);

  useEffect(()=>{
    setPageLoaded(true);
  },[]);

  return (
  <div>
  { (pageLoaded) ?
      <Component {...pageProps} />
      : null
    }
  </div>
  );
}

export default MyApp;
